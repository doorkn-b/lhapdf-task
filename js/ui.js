document.addEventListener('DOMContentLoaded', () => {
    const updatePlotAndEquation = () => {
        const a = parseFloat(document.getElementById('a-value').value);
        const b = parseFloat(document.getElementById('b-value').value);
        const c = parseFloat(document.getElementById('c-value').value);
        const xMin = parseFloat(document.getElementById('x-min').value);
        const xMax = parseFloat(document.getElementById('x-max').value);


        plotGraph(a, b, c, xMin, xMax); 


        let equation;
        if(a === 0){

            equation = `\\(y(x) = \\sin(${b}kx^{${c}})\\)`;
        } else {

            let formattedA = a < 0 ? `(${a})` : a;
            equation = `\\(y(x) = e^{-${formattedA}x} \\sin(${b}kx^{${c}})\\)`;
        }
        
        document.getElementById('dynamic-equation').innerHTML = equation;


        MathJax.typesetPromise();
    };

    const syncInputAndSlider = (inputId, sliderId) => {
        const input = document.getElementById(inputId);
        const slider = document.getElementById(sliderId);

        input.addEventListener('input', () => {
            slider.value = input.value;
            updatePlotAndEquation();
        });
        slider.addEventListener('input', () => {
            input.value = slider.value;
            adjustSliderStep(slider);
            updatePlotAndEquation();
        });
    };


    const adjustSliderStep = (slider) => {
        const value = parseFloat(slider.value);
        if (slider.id === 'a-slider') {
            slider.step = (value > 5 || value < -5) ? 0.5 : 0.1;
        } else if (slider.id === 'b-slider') {
            slider.step = value > 10 ? 1 : 0.1;
        } else if (slider.id === 'c-slider') {
            slider.step = value > 2 ? 0.2 : 0.05;
        }
    };

    syncInputAndSlider('a-value', 'a-slider');
    syncInputAndSlider('b-value', 'b-slider');
    syncInputAndSlider('c-value', 'c-slider');

    document.getElementById('set-range').addEventListener('click', updatePlotAndEquation);

    updatePlotAndEquation();
});
