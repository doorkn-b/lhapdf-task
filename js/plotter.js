function plotGraph(a, b, c, xMin, xMax) {
    const xValues = [];
    const yValues = [];
    for (let x = xMin; x <= xMax; x += (xMax - xMin) / 1000) {
        const y = Math.exp(-a * x) * Math.sin(b * Math.pow(x, c));
        xValues.push(x);
        yValues.push(y);
    }

    const trace = {
        x: xValues,
        y: yValues,
        mode: 'lines+markers',
        type: 'scatter',
        name: 'y(x)',
        line: {
            color: 'orange', 
            width: 3
        },
        marker: {
            color: 'red', 
            size: 3
        }
    };

    const layout = {
        title: 'Plot',
        xaxis: {
            title: 'x',
            showgrid: true, 
            zeroline: true,
        },
        yaxis: {
            title: 'y(x)',
            showgrid: true, 
            zeroline: true,
        },
        plot_bgcolor: '#fafafa', 
        paper_bgcolor: '#f5f5f5', 
        margin: { l: 50, r: 50, b: 50, t: 50, pad: 4 },
        autosize: true,
    };

    

    Plotly.newPlot('plotter', [trace], layout);
}
