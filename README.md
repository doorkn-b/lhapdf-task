# LHAPDF Evaluation Task - Arnab Mukherjee

Hey Andy and Christian, this is my task submission for the **MCnet/LHAPDF - online dashboard and data-visualisation for parton density functions** project. Any feedback is much appreciated.

## My Thinking Behind it:
I used HTML for the webpage structure, CSS for styling, and JavaScript for the plotting and interactions.

The JS files were divided based on functionality to keep the code organized and maintainable.
- `plotter.js` for the plotting logic
- `ui.js` for the UI interaction

I utilized two external libraries/frameworks for my structure, Plotly and MathJax.

I went with Plotly for a few particular reasons:
- It's a high level library that provides specific needs for plotting mathematical functions (very useful for our case).

- Its in-built functionality for plot controls-- these include panning, zooming, spike lines and comapring data.

- Plotly's ability to generate publication-quality plots. (Camera Option on the top-right of the plot.)
![Alt text](/Images/image-8.png)

- Having worked with Plotly before I can count on its documentation and community support.

MathJax was used to display mathematics in web browsers and rendering equations in LaTeX.

These were chosen ensuring that the application is lightweight while still offering powerful equation rendering capabilities.

## The Plotter:
Users can set the range of x and set the values of the variables to plot the graph. 
The inputs are parsed to ensure correct format.

To make the equation plotter interactive, I decided to implement real-time updating sliders for the variables. (In addition to being able to enter the values). 

This allows the user to adjust the sliders-- demonstrating how the gradual change of the variables affect the equation.

![Alt text](/Images/image-5.png)


![Alt text](/Images/image-6.png)

I added the ability to adjust the granularity of slider based on the current value. The motivation was to offer finer control when needed, and dealing with very sensitive sliders at higher value ranges:
```
const adjustSliderStep = (slider) => {
    const value = parseFloat(slider.value);
    if (slider.id === 'a-slider') {
        slider.step = (value > 5 || value < -5) ? 0.5 : 0.1;
    } else if (slider.id === 'b-slider') {
        slider.step = value > 10 ? 1 : 0.1;
    } else if (slider.id === 'c-slider') {
        slider.step = value > 2 ? 0.2 : 0.05;
    }
};

```

This variable sliding feature was also accompanied by a real-time equation at the bottom, portraying the exact equation currently plotted.

It makes sure to handle a double-negative situation and when A is set to 0.

![Alt text](/Images/image-2.png)

![Alt text](/Images/image-7.png)


This is the function I implemented that dynamically updates both the plot and the equation based on the inputs:
```
const updatePlotAndEquation = () => {
    const a = parseFloat(document.getElementById('a-value').value);
    const b = parseFloat(document.getElementById('b-value').value);
    const c = parseFloat(document.getElementById('c-value').value);
    const xMin = parseFloat(document.getElementById('x-min').value);
    const xMax = parseFloat(document.getElementById('x-max').value);

    plotGraph(a, b, c, xMin, xMax); 

    let equation;
    if(a === 0){
        equation = `\\(y(x) = \\sin(${b}kx^{${c}})\\)`;
    } else {
        let formattedA = a < 0 ? `(${a})` : a;
        equation = `\\(y(x) = e^{-${formattedA}x} \\sin(${b}kx^{${c}})\\)`;
    }
    
    document.getElementById('dynamic-equation').innerHTML = equation;

    MathJax.typesetPromise();
};
```
`MathJax.typesetPromise()` here ensures that any changes to the equation are rendered in mathematical notation.


### Plotting Logic:
Two arrays, `xValues` and `yValues`, are initialized to hold the x and y coordinates of the points that will be plotted. These arrays will be populated with the calculated values based on the specified function.


A for-loop iterates from `xMin` to `xMax`, incrementing by (`xmax`-`xmin`/100) each time to generate a smooth plot. For each 
x value in this range, it calculates the corresponding y value using the equation formula. These x and y values are then pushed to their respective arrays:
```
for (let x = xMin; x <= xMax; x += (xMax - xMin) / 1000) {
    const y = Math.exp(-a * x) * Math.sin(b * Math.pow(x, c));
    xValues.push(x);
    yValues.push(y);
}
```

A trace object is created, specifying how the data should be plotted. The x and y properties are set to the previously calculated arrays.

I chose the 'line+marker' mode-- to observe both the continuous nature of the function and individual data points. 

And it looks cooler while being lightweight :)

```
const trace = {
    x: xValues,
    y: yValues,
    mode: 'lines+markers',
    type: 'scatter',
    name: 'y(x)',
    line: {
        color: 'orange',
        width: 3
    },
    marker: {
        color: 'red',
        size: 3
    }
};

```

After this the plot layout was defined and the Plot was rendered:
```
Plotly.newPlot('plotter', [trace], layout);
```

## Backend Implementation:

Here is how I would approach implementing a plotting backend for functions which have to be evaluated in Python code rather than JavaScript. 


- To set-up the backend, I would go with either Django or Flask as my framework. They are both very robust. and I am also familiar with them.

- If it was a larger scale application with more built-in features, I'd go with Django. Flask is more lightweight, and serves as a good framework to just get started with.

- I would then create a RESTful API endpoint that accepts HTTP POST requests with parameters for the functions (example A, B, C and x range) in JSON format.

- When a request is recieved, I would validate the input parameters to avoid injection attacks or executing unintended code. Also make sure that the parameters are of the expected type and within reasonable bounds.
 We could also configure CORS policies to prevent unauthorized access to the API as an additional security measure if required.

- I would then use NumPy to compute the y values for a range of x values based on the provided function parameters. (Translating the mathematical function into a Python function)

- Then we can generate the plot using Matplotlib or Plotly. The plot data can be sent to the front end for rendering or we can generate an image to be sent back as a response. 
(If returning plot data, we will serialize the data as JSON for the front end.)



## Finishing Up:
This was a fun task to work on in the short time I had. I learnt more about the libraries and how I could implement the backend implementation methodology.

It'd be really cool to work on the PDF plotting tool for the PDF functions if given the opportunity.
I look foward to hearing any feedback.


Thanks a lot!


